import 'package:flutter/material.dart';
import 'package:fitness_app/allenamento.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:intl/intl.dart';
import 'package:pedometer/pedometer.dart';
import 'dart:async';
import 'package:permission_handler/permission_handler.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}


final descrizioneAllenamenti = [
  '3 CIRCUITI - 30/40 MIN',
  '2 CIRCUITI - 15/20 MIN',
  '3 CIRCUITI - 20/25 MIN'
];

class _HomePageState extends State<HomeScreen> {
  Stream<StepCount> _stepCountStream;
  Stream<PedestrianStatus> _pedestrianStatusStream;
  String _status = '?', _steps = '?';
  int calorie = 0;

  final imageAllenamenti = ['images/fullbody.jpg', 'images/Stretching.jpg', 'images/lowimpact.jpg'];
  final nomeAllenamenti = ['FULL BODY', 'STRETCHING', 'LOW IMPACT'];


  @override
  void initState() {
    super.initState();
    initPlatformState();
    askPermissions();
  }

  void onStepCount(StepCount event) {
    print(event);
    setState(() {
      _steps = event.steps.toString();
    });
  }

  void onPedestrianStatusChanged(PedestrianStatus event) {
    print(event);
    setState(() {
      _status = event.status;
    });
  }

  void onPedestrianStatusError(error) {
    print('onPedestrianStatusError: $error');
    setState(() {
      _status = 'Pedestrian Status not available';
    });
    print(_status);
  }

  void onStepCountError(error) {
    print('onStepCountError: $error');
    setState(() {
      _steps = 'Step Count not available';
    });
  }

  void initPlatformState() {
    _pedestrianStatusStream = Pedometer.pedestrianStatusStream;
    _pedestrianStatusStream
        .listen(onPedestrianStatusChanged)
        .onError(onPedestrianStatusError);

    _stepCountStream = Pedometer.stepCountStream;
    _stepCountStream.listen(onStepCount).onError(onStepCountError);

    if (!mounted) return;
  }

  @override
  Widget build(BuildContext context) {
    if (_steps != '?' &&
        _steps != 'Step Count not available' &&
        _steps != 'Pedestrian Status not available')
      calorie = (int.parse(_steps) * 0.004).round();
    return Container(
        child: SingleChildScrollView(
            child: Column(
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(8, 20, 8, 0),
          child: Text(
            'Allenamento',
            style: TextStyle(
                fontSize: 50,
                color: Colors.greenAccent[700],
                fontWeight: FontWeight.bold),
          ),
        ),
        Text(
          'del Giorno',
          style: TextStyle(
              fontSize: 50,
              color: Colors.greenAccent[700],
              fontWeight: FontWeight.bold),
        ),
        Padding(
            padding: EdgeInsets.all(15.0),
            child: Align(
                alignment: Alignment.center,
                child: Text(todayDate(),
                    style: TextStyle(fontStyle: FontStyle.italic)))),
        Row(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.fromLTRB(25, 8, 8, 8),
              child: Icon(
                Icons.local_fire_department_sharp,
                color: Colors.green,
                size: 30.0,
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                calorie.toString() + " Kcal",
                style: TextStyle(fontSize: 15),
              ),
            ),
            SizedBox(width: 50),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Icon(
                Icons.directions_run,
                color: Colors.green,
                size: 30.0,
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Text(
                _steps + " passi",
                style: TextStyle(fontSize: 15),
              ),
            ),
          ],
        ),
        SizedBox(height: 15),
        sliderAllenamenti(),
        SizedBox(height: 5),
      ],
    )));
  }

  sliderAllenamenti(){

    List numeri = (calorie > 300) ? [2,3] : [1];

    return CarouselSlider(
      options: CarouselOptions(height: 320.0),
      items: numeri.map((i) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.symmetric(horizontal: 10.0),
              decoration: BoxDecoration(
                color: Colors.white70,
                border: Border.all(
                  color: Colors.grey,
                  width: 1,
                ),
                borderRadius: BorderRadius.circular(12),
              ),
              child: GestureDetector(
                child: Column(children: <Widget>[
                  SizedBox(height: 10),
                  Text(
                    nomeAllenamenti[i - 1],
                    style: TextStyle(
                        fontSize: 22.0, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 8),
                  Image.asset(
                    imageAllenamenti[i - 1],
                    height: 230,
                    fit: BoxFit.fill,
                  ),
                  SizedBox(height: 10),
                  Text(
                    descrizioneAllenamenti[i - 1],
                    style: TextStyle(
                        fontSize: 16.0, fontStyle: FontStyle.italic),
                  ),
                ]),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Allenamento(
                              nomeAllenamento: nomeAllenamenti[i - 1],
                              descrizioneAllenamento:
                              descrizioneAllenamenti[i - 1],
                              imageAllenamento: imageAllenamenti[i - 1])));
                }, // handle your image tap here
              ),
            );
          },
        );
      }).toList(),
    );
  }

  todayDate() {
    var now = new DateTime.now();
    var formatter = new DateFormat('dd-MM-yyyy');
    String formattedDate = formatter.format(now);
    return formattedDate;
  }

  askPermissions() async {
    if (await Permission.activityRecognition.request().isGranted) {
      return;
    }
    await Permission.activityRecognition.request();
  }
}
