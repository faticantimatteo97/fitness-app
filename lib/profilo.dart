import 'package:fitness_app/homescreen.dart';
import 'package:fitness_app/main.dart';
import 'package:flutter/material.dart';

class Profilo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _StatefulWidgetDemoState();
  }
}

class _StatefulWidgetDemoState extends State<Profilo> {
  String name = " ";
  String age = " ";
  String heigth = " ";
  String weight = " ";

  _StatefulWidgetDemoState() {
    for (String par in ["name", "age", "heigth", "weight"]) {
      HomePage().getData(par).then((val) => setState(() {
            switch (par) {
              case "name":
                name = val;
                break;
              case "age":
                age = val;
                break;
              case "heigth":
                heigth = val;
                break;
              case "weight":
                weight = val;
                break;
            }
          }));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.green, Colors.greenAccent])),
                child: Container(
                  width: double.infinity,
                  height: 350.0,
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          backgroundImage: NetworkImage(
                            "https://media.discordapp.net/attachments/832295892341096528/855017938661408788/weightlifting.png",
                          ),
                          radius: 50.0,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          name,
                          style: TextStyle(
                            fontSize: 22.0,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Card(
                          margin: EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 5.0),
                          clipBehavior: Clip.antiAlias,
                          color: Colors.white,
                          elevation: 5.0,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 8.0, vertical: 22.0),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        "Età",
                                        style: TextStyle(
                                          color: Colors.green,
                                          fontSize: 22.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Text(
                                        age,
                                        style: TextStyle(
                                          fontSize: 20.0,
                                          color: Colors.green,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        "Altezza",
                                        style: TextStyle(
                                          color: Colors.green,
                                          fontSize: 22.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Text(
                                        heigth + "cm",
                                        style: TextStyle(
                                          fontSize: 20.0,
                                          color: Colors.green,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Expanded(
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        "Peso",
                                        style: TextStyle(
                                          color: Colors.green,
                                          fontSize: 22.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5.0,
                                      ),
                                      Text(
                                        weight + "kg",
                                        style: TextStyle(
                                          fontSize: 20.0,
                                          color: Colors.green,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )),
            Container(
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 30.0, horizontal: 16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Traguardi:",
                      style: TextStyle(
                          color: Colors.green,
                          fontStyle: FontStyle.normal,
                          fontSize: 28.0),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    buildTraguardi()
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  buildTraguardi() {
    if (HomeScreen().createState().calorie > 300) {
      return Container(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
        height: 90,
        width: double.maxFinite,
        child: Card(
            elevation: 3,
            child: Row(
              children: <Widget>[
                SizedBox(
                  width: 10,
                ),
                Text(
                  "Nessun traguardo raggiunto",
                  style: TextStyle(
                      color: Colors.green,
                      fontStyle: FontStyle.normal,
                      fontSize: 22.0),
                ),
              ],
            )),
      );
    } else {
      return Column(children: <Widget>[
        Container(
          padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
          height: 90,
          width: double.maxFinite,
          child: Card(
              elevation: 3,
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Passi giornalieri: 8000",
                    style: TextStyle(
                        color: Colors.green,
                        fontStyle: FontStyle.normal,
                        fontSize: 22.0),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Icon(
                    Icons.emoji_events,
                    color: Colors.green,
                    size: 30.0,
                  ),
                ],
              )),
        ),
        SizedBox(
          height: 3.0,
        ),
        Container(
          padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
          height: 90,
          width: double.maxFinite,
          child: Card(
              elevation: 3,
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Calorie bruciate: 300",
                    style: TextStyle(
                        color: Colors.green,
                        fontStyle: FontStyle.normal,
                        fontSize: 22.0),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Icon(
                    Icons.emoji_events,
                    color: Colors.green,
                    size: 30.0,
                  ),
                ],
              )),
        )
      ]);
    }
  }
}
