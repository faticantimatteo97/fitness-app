import 'package:flutter/material.dart';
import 'package:confetti/confetti.dart';

class ConfettiSample extends StatelessWidget {
  const ConfettiSample({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Confetti',
        home: Scaffold(
          backgroundColor: Colors.grey[900],
          body: FineAllenamento(),
        ));
  }
}

class FineAllenamento extends StatefulWidget {
  @override
  _FineAllenamentoState createState() => _FineAllenamentoState();
}

class _FineAllenamentoState extends State<FineAllenamento> {
  //ConfettiController _controllerCenter;
  //ConfettiController _controllerCenterRight;
  //ConfettiController _controllerCenterLeft;
  ConfettiController _controllerTopCenter;
  //ConfettiController _controllerBottomCenter;

  @override
  void initState() {
    _controllerTopCenter =
        ConfettiController(duration: const Duration(seconds: 5));
    _controllerTopCenter.play();
    super.initState();
  }

  @override
  void dispose() {
    _controllerTopCenter.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.green, Colors.greenAccent],
            begin: FractionalOffset.topCenter,
            end: FractionalOffset.bottomCenter,
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ConfettiWidget(
              confettiController: _controllerTopCenter,
              blastDirectionality: BlastDirectionality.explosive,
              particleDrag: 0.05,
              emissionFrequency: 0.10,
              numberOfParticles: 10,
              gravity: 0.10,
              shouldLoop: false,
              colors: const [
                Colors.green,
                Colors.blue,
                Colors.pink,
                Colors.orange,
                Colors.purple
              ], // manually specify the colors to be used
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 0, 10, 55),
              child: Align(
                  alignment: Alignment.topRight,
                  child: GestureDetector(
                    onTap: () => {
                      Navigator.pushNamedAndRemoveUntil(
                          context, "/homepage", (route) => false)
                    }, // handle your image tap h
                    // ere
                    child: Image.asset('images/x.png',
                        scale: 25, alignment: Alignment.topRight),
                  )),
            ),
            Padding(
              padding: EdgeInsets.all(15.0),
              child: Align(
                alignment: Alignment.center,
                child: Text('OTTIMO LAVORO!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 43.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        decoration: TextDecoration.none)),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(15.0),
              child: Align(
                alignment: Alignment.center,
                child: Text('Hai terminato il tuo allenamento',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                        decoration: TextDecoration.none)),
              ),
            ),
            Image(image: AssetImage('images/medalApp.png')),
          ],
        ));
  }
}
