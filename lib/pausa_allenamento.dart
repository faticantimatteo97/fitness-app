import 'package:fitness_app/allenamento.dart';
import 'package:flutter/material.dart';
import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:fitness_app/inizio_allenamento.dart';

class PausaAllenamento extends StatefulWidget {
  @override
  _PausaAllenamentoState createState() => _PausaAllenamentoState();

  final nomeAllenamento;
  final nCircuito;

  const PausaAllenamento({Key key, this.nomeAllenamento, this.nCircuito})
      : super(key: key);
}

class _PausaAllenamentoState extends State<PausaAllenamento> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String nomeAllenamento = widget.nomeAllenamento;
    int nCircuito = widget.nCircuito + 1;
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
            child: Text('RIPOSATI!',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 43.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    decoration: TextDecoration.none)),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
            child: Text('Prossimo circuito tra:',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    decoration: TextDecoration.none)),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: CountdownFormatted(
                duration: Duration(seconds: 30),
                builder: (BuildContext ctx, String remaining) {
                  return Text(
                    remaining,
                    style: TextStyle(
                        fontSize: 100.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        decoration: TextDecoration.none),
                  ); // 01:00:00
                },
                onFinish: () {
                  print('finito');
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => InizioAllenamento(
                              nomeAllenamento: nomeAllenamento,
                              nCircuito: nCircuito)));
                }),
          ),
          SizedBox(
            height: 50,
          ),
          Text(
            "A seguire: ",
            style: TextStyle(
                fontSize: 22.0,
                fontStyle: FontStyle.italic,
                fontWeight: FontWeight.w300,
                color: Colors.black,
                letterSpacing: 2.0,
                decoration: TextDecoration.none),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            Scheda().getEsercizioCircuitoScheda(nomeAllenamento, nCircuito, 0),
            style: TextStyle(
                fontSize: 30.0,
                fontStyle: FontStyle.italic,
                fontWeight: FontWeight.bold,
                color: Colors.black,
                letterSpacing: 2.0,
                decoration: TextDecoration.none),
          ),
          Card(
            semanticContainer: true,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            child: Image(
              image: AssetImage(Scheda().getImage(Scheda()
                  .getEsercizioCircuitoScheda(nomeAllenamento, nCircuito,
                      0))), //Scheda().mapImage.values.elementAt(round)),
              height: 200,
              width: 300,
              fit: BoxFit.fill,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            elevation: 5,
            margin: EdgeInsets.all(10),
          ),
        ],
      ),
    );
  }
}
