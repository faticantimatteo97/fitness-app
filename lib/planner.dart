import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

class Planner extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Planner();
}

class _Planner extends State<Planner> {
  List<Appointment> meetings = <Appointment>[];
  Appuntamento value;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SfCalendar(
        view: CalendarView.month,
        monthViewSettings: MonthViewSettings(
          showAgenda: true,
        ),
        initialSelectedDate: DateTime.now(),
        dataSource: MeetingDataSource(getApp()),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          value = await Navigator.push(
            context,
            MaterialPageRoute<Appuntamento>(
                builder: (context) => RegistrazioneAllenamento()),
          );
          setState(() {
            getAppointments();
          });
        },
        child: const Icon(Icons.add),
        backgroundColor: Colors.green,
      ),
    );
  }

  List<Appointment> getApp() {
    return meetings;
  }

  Future getAppointments() async {
    final DateTime startTime = DateTime(value.data.year, value.data.month,
        value.data.day, value.orario.hour, value.orario.minute, 0);
    final DateTime endTime = startTime.add(const Duration(hours: 1));
    BestTutorSite type = value.tipo;
    String t;
    Color colore;
    if (type == BestTutorSite.Cardio) {
      t = 'Cardio';
      colore = Colors.orange;
    } else if (type == BestTutorSite.Recupero) {
      t = 'Recupero';
      colore = Colors.purple;
    } else if (type == BestTutorSite.Resistenza) {
      t = 'Resistenza';
      colore = Colors.green;
    } else {
      t = 'Non è stato selezionato il tipo di allenamento';
      colore = Colors.red;
    }
    meetings.add(Appointment(
        startTime: startTime, endTime: endTime, subject: t, color: colore));
    return meetings;
  }
}

class Appuntamento {
  Appuntamento(this.data, this.orario, this.tipo);
  final DateTime data;
  final TimeOfDay orario;
  final BestTutorSite tipo;
}

class MeetingDataSource extends CalendarDataSource {
  MeetingDataSource(List<Appointment> source) {
    appointments = source;
  }
}

enum BestTutorSite { Cardio, Resistenza, Recupero }

class RegistrazioneAllenamento extends StatefulWidget {
  RegistrazioneAllenamento({Key key}) : super(key: key);

  @override
  _RegistrazioneAllenamentoState createState() =>
      _RegistrazioneAllenamentoState();
}

class _RegistrazioneAllenamentoState extends State<RegistrazioneAllenamento> {
  DateTime date;
  TimeOfDay time;
  BestTutorSite type;
  Appuntamento appointment;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text("Planner"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                  child: SingleChildScrollView(
                      child: Column(children: [
                Padding(
                    padding: EdgeInsets.fromLTRB(8, 20, 8, 0),
                    child: Text(
                      'Registrazione',
                      style: TextStyle(
                          fontSize: 50,
                          color: Colors.greenAccent[700],
                          fontWeight: FontWeight.bold),
                    )),
                Text(
                  'allenamento',
                  style: TextStyle(
                      fontSize: 50,
                      color: Colors.greenAccent[700],
                      fontWeight: FontWeight.bold),
                )
              ]))),
              Card(
                child: ListTile(
                  onTap: () => showDialog<String>(
                    context: context,
                    builder: (BuildContext context) => AlertDialog(
                      title: const Text('Tipo allenamento'),
                      //backgroundColor: Colors.green,
                      //height: 50,
                      content: Column(
                        children: <Widget>[
                          Card(
                            child: ListTile(
                              title: Text('Cardio'),
                              //trailing: Icon(Icons.arrow_forward_ios_rounded),
                              onTap: () => setState(() {
                                type = BestTutorSite.Cardio;
                                appointment = Appuntamento(
                                    date, time, BestTutorSite.Cardio);
                                Navigator.pop(context);
                              }),
                            ),
                          ),
                          Card(
                            child: ListTile(
                              title: Text('Resistenza'),
                              //trailing: Icon(Icons.arrow_forward_ios_rounded),
                              onTap: () => setState(() {
                                type = BestTutorSite.Resistenza;
                                appointment = Appuntamento(
                                    date, time, BestTutorSite.Resistenza);
                                Navigator.pop(context);
                              }),
                            ),
                          ),
                          Card(
                            child: ListTile(
                              title: Text('Recupero'),
                              //trailing: Icon(Icons.arrow_forward_ios_rounded),
                              onTap: () => setState(() {
                                type = BestTutorSite.Recupero;
                                appointment = Appuntamento(
                                    date, time, BestTutorSite.Recupero);
                                Navigator.pop(context);
                              }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  leading: Icon(Icons.directions_run_rounded),
                  title: Text('Tipo Allenamento'),
                  subtitle: Text(getType()),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                ),
              ),
              Card(
                child: ListTile(
                  leading: Icon(Icons.calendar_today),
                  title: Text('Data Allenamento'),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                  onTap: () => pickDate(context),
                  subtitle: Text(getDate()),
                ),
              ),
              Card(
                child: ListTile(
                  leading: Icon(Icons.watch_later_rounded),
                  title: Text('Orario Allenamento'),
                  trailing: Icon(Icons.arrow_forward_ios_rounded),
                  onTap: () => pickTime(context),
                  subtitle: Text(getTime()),
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                width: 350,
                height: 50,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [Colors.green, Colors.greenAccent],
                      begin: FractionalOffset.centerLeft,
                      end: FractionalOffset.centerRight,
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                // ignore: deprecated_member_use
                child: FlatButton(
                    child: Text("Inserisci allenamento",
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white)),
                    onPressed: () => {
                          /* if (appointment.data == null ||
                              appointment.orario == null)
                            {
                             showDialog<String>(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                  title: const Text('AlertDialog Title'),
                                  content:
                                      const Text('AlertDialog description'),
                                  actions: <Widget>[
                                    TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, 'Chiudi'),
                                      child: const Text('Chiudi'),
                                    ),
                                  ],
                                ),
                              )
                            }
                          else*/
                          {Navigator.pop(context, appointment)}
                        }),
              ),
            ],
          ),
        ));
  }

  String getType() {
    if (type == BestTutorSite.Cardio) {
      return 'Cardio';
    } else if (type == BestTutorSite.Recupero) {
      return 'Recupero';
    } else if (type == BestTutorSite.Resistenza) {
      return 'Resistenza';
    } else {
      return 'Seleziona il tipo allenamento';
    }
  }

  Future pickDate(BuildContext context) async {
    final initialDate = DateTime.now();
    final newDate = await showDatePicker(
        context: context,
        initialDate: initialDate,
        firstDate: DateTime(DateTime.now().year),
        lastDate: DateTime(DateTime.now().year + 1));

    if (newDate == null) return;

    setState(() {
      date = newDate;
      appointment = Appuntamento(newDate, time, type);
    });
  }

  String getDate() {
    if (date == null) {
      return 'Seleziona Data';
    } else {
      return '${date.month}/${date.day}/${date.year}';
    }
  }

  String getTime() {
    if (time == null) {
      return 'Seleziona Orario';
    } else {
      final hours = time.hour.toString().padLeft(2, '0');
      final minute = time.minute.toString().padLeft(2, '0');
      return '$hours:$minute';
    }
  }

  Future pickTime(BuildContext context) async {
    final initialTime = TimeOfDay(hour: 9, minute: 0);
    final newTime = await showTimePicker(
      context: context,
      initialTime: time ?? initialTime,
    );

    if (newTime == null) return;
    setState(() {
      time = newTime;
      appointment = Appuntamento(date, newTime, type);
    });
  }
}
