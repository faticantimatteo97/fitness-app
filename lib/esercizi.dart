import 'package:fitness_app/allenamento.dart';
import 'package:flutter/material.dart';

class Esercizi extends StatelessWidget {
  final titles = [
    "Esercizi Addominali",
    "Esercizi Pettorali",
    "Esercizi Dorsali",
    "Esercizi Gambe",
    "Esercizi Spalle",
    "Esercizi Bicipiti"
  ];
  final images = [
    'images/addominali.png',
    'images/pettorali1.jpg',
    'images/dorsali.jpg',
    'images/gambe.png',
    'images/spalle.jpg',
    'images/bicipiti.png'
  ];

  final esercizi = [
    ['images/addominale 1.gif', 'images/addominale 2.gif', 'images/addominale 3.gif', 'images/mountainclimbers.gif', 'images/plank.gif', 'images/sideplank.gif', 'images/frozen-v-sit.gif', 'images/armplank.gif', 'images/deadbug.gif', 'images/crunches.gif', 'images/crunch.gif'],
    ['images/petto 1.gif', 'images/petto 2.gif', 'images/petto 3.gif', 'images/pushup.gif', 'images/inclinebbbench.gif', 'images/floorpress.gif', 'images/diamond.gif'],
    ['images/dorsali 3.gif', 'images/dorsali 1.gif', 'images/dorsali 2.gif', 'images/pronebackextension.gif', 'images/bent-over-row.gif', 'images/rematorepanca.gif', 'images/renegaderowdb.gif', 'images/superman2.gif'],
    ['images/squat.gif', 'images/Walking Lunge.gif', 'images/ponte2.gif', 'images/gambe 3.gif', 'images/gambe 2.gif', 'images/gambe 1.gif', 'images/affondibilanciere.gif', 'images/squatmanubri.gif', 'images/affondisalto.gif', 'images/ponte.gif', 'images/stacchirumeni.gif', 'images/alzatabilanciere.gif'],
    ['images/kettlbellswing.gif', 'images/pike.gif', 'images/spalle 1.gif', 'images/spalle 2.gif', 'images/spalle 3.gif', 'images/alzatementosingolo.gif', 'images/seatedshoulder.gif', 'images/dumbbellpress.gif'],
    ['images/bicipiti 3.gif', 'images/concentrationcurl.gif', 'images/TwistingDumbbellCurl.gif', 'images/reversecurlgif.gif', 'images/leanEZgif.gif', 'images/reverseEZ.gif', 'images/ProneDumbbellSpiderCurl.gif', 'images/bicipiti 1.gif', "images/bicepcurl.gif"]
  ];

  final List nomiEsercizi = [
    ["Addominali laterali", "Crunch inverso", "Crunch a mani tese", 'Mountain climbers', 'Plank', 'Plank laterale', 'V sit', 'Extended Plank', "Deadbug", "Crunch", "Sit up"],
    ["Panca piana con bilanciere", "Panca piana con bilanciere con presa stretta", "Panca piana con manubri", "Push up", "Panca inclinata con bilanciere", "Pressa con manubri da terra", "Diamond"],
    ["Rematore con bilanciere", "Rematore singolo con manubrio", "Rematore singolo con barra", "Dorsali bassi a terra", "Rematore inclinato con manubri", "Rematore su panca", "Renegade", "Superman"],
    ["Squat", "Affondi in camminata", "Ponte con gamba singola", 'Sumo Squat con Kettlbell', 'Squat con bilanciere esplosivi', 'Stacchi da terra con gambe tese', 'Affondi con bilanciere', "Squat con manubri", "Affondi alternati con salto", "Ponte", "Stacchi rumeni", "Stacchi esplosivi con bilanciere"],
    ["Swing con kettbell", "Pike", "Alzate laterali", "Spinte con bilanciere", "Alzate al mento", "Alzate al mento singolo", "Pressa spalle da seduto", "Pressa con manubri"],
    ["Curl con bicipiti inclinati", "Curl concentrato", "Dumbbell curl alternato", "Curl inversi con barra", "Curl con barra EZ inclinato in avanti", "Curl con barra EZ grip inverso", "Curl su panca prono con manubri", "Curl a trascinamento con barra EZ", "Curl con manubri"],
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: titles.length,
        itemBuilder: (context, index) {
          return Card(
              child: ListTile(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SecondRoute(
                            title: titles[index],
                              esercizi: esercizi[index],
                              nomiEsercizi: nomiEsercizi[index])),
                    );
                  },
                  title: Text(titles[index]),
                  trailing: Image(image: AssetImage(images[index]))));
        });
  }
}

class SecondRoute extends StatelessWidget {
  final List esercizi;
  final List nomiEsercizi;
  final String title;

  const SecondRoute({Key key, this.title, this.esercizi, this.nomiEsercizi})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text("Lista " + title),
        ),
        body: ListView.builder(
            itemCount: esercizi.length,
            itemBuilder: (context, index) {
              return Card(
                  child: ListTile(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DisplayEsercizio(
                                    nomeEsercizio: nomiEsercizi[index],
                                    img: esercizi[index]
                                  )),
                        );
                      },
                      title: Text(nomiEsercizi[index]),
                      trailing: Image(image: AssetImage(esercizi[index]))));
            }));
  }
}

class DisplayEsercizio extends StatelessWidget {
  final nomeEsercizio;
  final img;
  const DisplayEsercizio({Key key, this.nomeEsercizio, this.img}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String des = new Scheda().getDescrizione(nomeEsercizio);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text(nomeEsercizio == null ? "" : nomeEsercizio),
        ),
        body: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 30.0, horizontal: 16.0),
            child: Column(children: <Widget>[
              Card(
                semanticContainer: true,
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child: Image(
                  image: AssetImage(img),
                  height: 250,
                  width: 300,
                  fit: BoxFit.fill,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                elevation: 5,
                margin: EdgeInsets.all(10),
              ),
              SizedBox(height: 10),
              Text(
                des,
                style: TextStyle(
                  fontSize: 20.0,
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.w300,
                  color: Colors.black,
                  letterSpacing: 2.0,
                ),
              )
            ])));
  }
}
