import 'package:firebase_auth/firebase_auth.dart';
import 'package:fitness_app/email_login.dart';
import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';

class Impostazioni extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text("Impostazioni"),
      ),
      body: SettingsList(
        sections: [
          SettingsSection(
            title: 'Generali',
            tiles: [
              SettingsTile(
                title: 'Lingua',
                subtitle: 'Italiano',
                leading: Icon(Icons.language),
              ),
            ],
          ),
          SettingsSection(
            title: 'Account',
            tiles: [
              SettingsTile(title: 'Email', leading: Icon(Icons.email)),
              SettingsTile(
                  title: 'Sign out',
                  leading: Icon(Icons.exit_to_app),
                  // ignore: deprecated_member_use
                  onTap: () {
                    FirebaseAuth auth = FirebaseAuth.instance;
                    auth.signOut().then((res) {
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(builder: (context) => EmailLogIn()),
                          (Route<dynamic> route) => false);
                    });
                  }),
            ],
          ),
          SettingsSection(
            title: 'Security',
            tiles: [
              SettingsTile.switchTile(
                title: 'Change password',
                leading: Icon(Icons.lock),
                switchValue: true,
                onToggle: (bool value) {},
              ),
            ],
          ),
          SettingsSection(
            title: 'Misc',
            tiles: [
              SettingsTile(
                  title: 'Termini di Servizio',
                  leading: Icon(Icons.description)),
              SettingsTile(
                  title: 'Open source licenses',
                  leading: Icon(Icons.collections_bookmark)),
            ],
          )
        ],
      ),
    );
  }
}
