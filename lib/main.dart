import 'package:fitness_app/email_login.dart';
import 'package:fitness_app/esercizi.dart';
import 'package:fitness_app/homescreen.dart';
import 'package:fitness_app/impostazioni.dart';
import 'package:fitness_app/planner.dart';
import 'package:fitness_app/profilo.dart';
import 'package:fitness_app/progressi.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FitApp',
      theme: ThemeData(
        primarySwatch: Colors.green,
        brightness: Brightness.light,
      ),
      home: IntroScreen(),
      routes: {
        '/home': (context) => HomeScreen(),
        '/homepage': (context) => HomePage()
      },
    );
  }
}

class HomePage extends StatefulWidget {
  HomePage({this.uid});
  final FirebaseAuth auth = FirebaseAuth.instance;
  final String uid;
  final String title = "Home";
  final databaseRef = FirebaseDatabase.instance.reference();

  Future<String> getData(String valore) async {
    String userId = (FirebaseAuth.instance.currentUser).uid;
    return databaseRef
        .child('Users')
        .child(userId)
        .once()
        .then((DataSnapshot snapshot) {
      print(userId);
      return (snapshot.value[valore]);
    });
  }

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int selectedPage = 2;

  final _pageOptions = [
    Progressi(),
    Planner(),
    HomeScreen(),
    Esercizi(),
    Profilo()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text('FitApp'),
            backgroundColor: Colors.green,
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.settings),
                tooltip: 'Show Snackbar',
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Impostazioni()),
                  );
                },
              ),
            ]),
        backgroundColor: Colors.white,
        body: _pageOptions[selectedPage],
        bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.trending_up, size: 30), label: 'Progressi'),
            BottomNavigationBarItem(
                icon: Icon(Icons.date_range, size: 30), label: 'Planner'),
            BottomNavigationBarItem(
                icon: Icon(Icons.home, size: 30), label: 'Home'),
            BottomNavigationBarItem(
                icon: Icon(Icons.fitness_center, size: 30), label: 'Esercizi'),
            BottomNavigationBarItem(
                icon: Icon(Icons.account_circle, size: 30), label: 'Profilo'),
          ],
          selectedItemColor: Colors.green,
          elevation: 5.0,
          unselectedItemColor: Colors.green[900],
          currentIndex: selectedPage,
          backgroundColor: Colors.white,
          onTap: (index) {
            setState(() {
              selectedPage = index;
            });
          },
        ));
  }
}

class IntroScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    User result = FirebaseAuth.instance.currentUser;
    return new SplashScreen(
        navigateAfterSeconds:
            result != null ? HomePage(uid: result.uid) : EmailLogIn(),
        seconds: 5,
        title: new Text(
          'Benvenuto su FitApp!',
          style: new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
        ),
        //image:
        //Image.asset('images/retto-addominale.png', fit: BoxFit.scaleDown),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        onClick: () => print("flutter"),
        loaderColor: Colors.red);
  }
}
