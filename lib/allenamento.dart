import 'package:flutter/material.dart';
import 'package:fitness_app/inizio_allenamento.dart';

class Allenamento extends StatelessWidget {
  final String nomeAllenamento;
  final String descrizioneAllenamento;
  final String imageAllenamento;

  const Allenamento(
      {Key key,
      this.nomeAllenamento,
      this.descrizioneAllenamento,
      this.imageAllenamento})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Scheda scheda = new Scheda();
    final Map<String, List> s = scheda.getScheda(nomeAllenamento);
    final Map<String, String> g = scheda.getGiri(nomeAllenamento);
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text("Allenamento"),
        ),
        body: Stack(children: <Widget>[
          ListView.builder(
              itemCount: s.length,
              itemBuilder: (context, index) {
                if (index == 0) {
                  return Column(
                    children: [
                      // The header
                      Image.asset(
                        imageAllenamento,
                      ),
                      SizedBox(height: 10),
                      Text(nomeAllenamento,
                          style: TextStyle(
                              fontSize: 35.0, fontWeight: FontWeight.bold)),
                      Text(descrizioneAllenamento,
                          style: TextStyle(
                              fontSize: 15.0, fontStyle: FontStyle.italic)),
                      SizedBox(height: 10),
                      // The fist list item
                      Card(
                          child: Column(children: [
                        Text(s.keys.elementAt(index),
                            style: TextStyle(
                                fontSize: 20.0, fontWeight: FontWeight.bold)),
                        Text(g.values.elementAt(index),
                            style: TextStyle(
                                fontSize: 15.0, fontStyle: FontStyle.italic)),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            for (var item in s.values.elementAt(index))
                              ListTile(
                                  title: Text(item),
                                  subtitle: Text(scheda.getRipetizioni(item)),
                                  trailing: Image(
                                      image:
                                          AssetImage(scheda.getImage(item)))),
                          ],
                        ),
                      ])),
                    ],
                  );
                }
                // If index != 0
                else if (index == s.length - 1) {
                  return Card(
                      child: Column(children: [
                    Text(
                      s.keys.elementAt(index),
                      style: TextStyle(
                          fontSize: 20.0, fontWeight: FontWeight.bold),
                      textAlign: TextAlign.left,
                    ),
                    Text(g.values.elementAt(index),
                        style: TextStyle(
                            fontSize: 15.0, fontStyle: FontStyle.italic)),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        for (var item in s.values.elementAt(index))
                          ListTile(
                              title: Text(item),
                              subtitle: Text(scheda.getRipetizioni(item)),
                              trailing: Image(
                                  image: AssetImage(scheda.getImage(item))))
                      ],
                    ),
                    SizedBox(height: 75),
                  ]));
                }
                return Card(
                    child: Column(children: [
                  Text(
                    s.keys.elementAt(index),
                    style:
                        TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.left,
                  ),
                  Text(g.values.elementAt(index),
                      style: TextStyle(
                          fontSize: 15.0, fontStyle: FontStyle.italic)),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      for (var item in s.values.elementAt(index))
                        ListTile(
                            title: Text(item),
                            subtitle: Text(scheda.getRipetizioni(item)),
                            trailing:
                                Image(image: AssetImage(scheda.getImage(item))))
                    ],
                  ),
                ]));
              }),
        Positioned(
            width: 40,
            height: 40,
            right: 10,
            top: 10,
            child: Container(
              child:
                Tooltip(
                message: '\nComandi Vocali:\nProssimo: passa al prossimo esercizio\nIndietro: torna all esercizio precedente\nTermina: termina l allenamento\n',
                child: Image.asset('images/info2.png', alignment: Alignment.topLeft)
                )
            )
          ),
          Positioned(
            width: MediaQuery.of(context).size.width - 50,
            height: 70.0,
            left: (MediaQuery.of(context).size.width / 2) -
                ((MediaQuery.of(context).size.width - 50) / 2),
            bottom: 20,
            child: Container(
              margin: EdgeInsets.all(10),
              width: 300,
              height: 150,
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [Colors.green, Colors.greenAccent],
                    begin: FractionalOffset.centerLeft,
                    end: FractionalOffset.centerRight,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(20))),
              // ignore: deprecated_member_use
              child: FlatButton(
                child: Text("Inizia allenamento",
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: Colors.white)),
                onPressed: () => {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InizioAllenamento(
                            nomeAllenamento: nomeAllenamento, nCircuito: 1)),
                  )
                },
              ),
            ),
          )
        ]));
  }
}

class Scheda {
  final List allenamenti = ['FULL BODY', 'STRETCHING', 'LOW IMPACT'];
  final Map<String, Map<String, List>> mapSchede = {
    'FULL BODY': {
      'Circuito 1': [
        'Kettlbell swing',
        'Squat',
        'Affondi in camminata',
        'Ponte gamba singola'
      ],
      'Circuito 2': ['Burpees', 'Petto', 'Spalle', 'Bicipiti'],
      'Circuito 3': [
        'Mountain Climbers',
        'Long arm crunches',
        'Addominali laterali',
        'Crunch inverso'
      ]
    },
    'STRETCHING': {
      'Circuito 1': [
        'Bicipiti femorali',
        'Quadricipiti',
        'Flessori dell anca',
        'Glutei'
      ],
      'Circuito 2': [
        'Superman',
        'Allungamento spalla',
        'Allungamento tricipidi',
        'Addome'
      ]
    },
    'LOW IMPACT': {
      'Circuito 1': ['Corda', 'Deadbug', 'Ponte'],
      'Circuito 2': ['Mountain Climbers', 'Squat', 'Superman braccia unite'],
      'Circuito 3': ['Bear Crawl', 'Plank', 'Affondi in camminata']
    }
  };
  final Map<String, String> mapRipetizioni = {
    'Kettlbell swing': '10 volte',
    'Squat': '10 volte',
    'Affondi in camminata': '10 volte (5 per lato)',
    'Ponte gamba singola': '16 volte (8 per lato)',
    'Burpees': '8 volte',
    'Petto': '8 volte',
    'Spalle': '10 volte',
    'Bicipiti': '12 volte',
    'Mountain Climbers': '20 volte (10 per lato)',
    'Long arm crunches': '15 volte',
    'Addominali laterali': '24 volte (12 per lato)',
    'Crunch inverso': '15 volte',
    'Bicipiti femorali': '30 secondi',
    'Quadricipiti': '30 secondi',
    "Flessori dell anca": '30 secondi',
    'Glutei': '30 secondi',
    'Superman': '30 secondi',
    'Allungamento spalla': '30 secondi',
    'Allungamento tricipidi': '30 secondi',
    'Addome': '30 secondi',
    'Corda': '30 secondi',
    'Deadbug': '20 volte (10 per lato)',
    'Ponte': '16 volte',
    'Superman braccia unite': '12 volte',
    'Bear Crawl': '20 volte (10 per lato)',
    'Plank': '30 secondi'
  };

  final Map<String, String> mapImage = {
    'Kettlbell swing': 'images/kettlbellswing.gif',
    'Squat': 'images/squat.gif',
    'Affondi in camminata': 'images/Walking Lunge.gif',
    'Ponte gamba singola': 'images/ponte2.gif',
    'Burpees': 'images/burpees.gif',
    'Petto': 'images/petto 1.gif',
    'Spalle': 'images/spalle 3.gif',
    'Bicipiti': 'images/bicipiti 1.gif',
    'Mountain Climbers': 'images/mountainclimbers.gif',
    'Long arm crunches': 'images/addominale 3.gif',
    'Addominali laterali': 'images/addominale 1.gif',
    'Crunch inverso': 'images/addominale 2.gif',
    'Bicipiti femorali': 'images/s_polpacci.gif',
    'Quadricipiti': 'images/s_quadricipidi.gif',
    'Flessori dell anca': 'images/s_flessori.gif',
    'Glutei': 'images/s_glutei.gif',
    'Allungamento spalla': 'images/s_spalla.gif',
    'Allungamento tricipidi': 'images/s_tricipidi.gif',
    'Deltoidi': 'images/addominale 1.gif',
    'Addome': 'images/addominale 2.gif',
    'Superman': 'images/superman.gif',
    'Pike': 'images/pike.gif',
    'Corda': 'images/corda.gif',
    'Deadbug': 'images/deadbug.gif',
    'Ponte': 'images/ponte.gif',
    'Superman braccia unite': 'images/superman2.gif',
    'Bear Crawl': 'images/bear_crawl.gif',
    'Plank': 'images/plank.gif'
  };

  final Map<String, String> des = {
    'Addominali laterali':
        'Sdraiati su un fianco con la gamba destra sopra la sinistra. Metti la mano destra dietro la testa, il gomito allargato e l altra sul pavimento di fronte a te per bilanciare. Contrai gli obliqui per unire la gamba destra e il gomito, quindi abbassati lentamente. Esegui tutte le ripetizioni su un lato prima di scambiare.',
    "Crunch inverso":
        "Sdraiati sulla schiena con le braccia a terra lungo i fianchi, i palmi rivolti verso il basso. Piega le ginocchia e avvicinale al petto contraendo gli addominali.  Mentre si alzano, ruota il bacino per sollevare i fianchi dal pavimento. Spremere in alto, quindi abbassare lentamente fino a quando le cosce non sono perpendicolari al pavimento.",
    "Crunch a mani tese":
        "Sdraiati sulla schiena con le ginocchia piegate e le braccia raddrizzate dietro di te. Quindi, tenendo le braccia dritte sopra la testa, esegui un tradizionale crunch. Il movimento deve essere lento e controllato.",
    'Mountain climbers':
        "Assumi una posizione di flessione con le mani sotto le spalle e il tuo corpo formando una linea retta dalla testa ai talloni. Questa è la posizione di partenza. Sollevando il piede destro dal pavimento, porta il ginocchio destro verso il petto. Tocca il pavimento con il piede destro e poi torna alla posizione di partenza. Gambe alternate ad ogni ripetizione",
    'Plank':
        "Mettiti in una posizione di piegamento, ma riposa sugli avambracci piuttosto che sulle mani. Assicurati che la schiena sia dritta e contrai addominali e glutei. Tieni senza che i fianchi si abbassino.",
    'Plank laterale':
        'Sdraiati sul lato sinistro con le ginocchia dritte e solleva la parte superiore del corpo per sostenere il suo peso sull avambraccio. Sostieni il tuo nucleo e solleva i fianchi finché il tuo corpo non forma una linea retta. Mantieni questa posizione respirando profondamente. Quindi rotolare e ripetere dall altro lato',
    'V sit':
        'Sdraiati sulla schiena con le braccia e le gambe distese e le mani e i piedi sollevati appena sopra il pavimento. Inizia l esercizio sollevando contemporaneamente il busto e le gambe fino a toccare i piedi. Tenere per il tempo richiesto.',
    'Extended Plank':
        'Mettiti in una posizione di flessione, posizionando le mani a circa 10 pollici davanti alle spalle, con le punte delle scarpe appoggiate al pavimento. Mantieni questa posizione con la schiena dritta e cerca di continuare a respirare normalmente.',
    "Panca piana con bilanciere":
        "Sdraiati su una panca piana tenendo un bilanciere nel rack sopra di te con una presa prona alla larghezza delle spalle. Sollevare la barra dal rack e posizionarla sopra il petto con le braccia completamente distese. Dalla posizione di partenza, inspira e abbassa lentamente la barra fino a sfiorare il centro del petto. Spingi la barra indietro nella posizione di partenza in modo esplosivo mentre espiri.",
    "Panca piana con bilanciere con presa stretta":
        "Afferrare un bilanciere con una presa prona a 15-30 cm di distanza e tenerlo sopra lo sterno con le braccia completamente tese. Abbassa lentamente la barra fino al petto. Assicurati di tenere i gomiti piegati vicino ai fianchi, la parte superiore delle braccia formando un angolo di 45 gradi rispetto al corpo. Metti in pausa, quindi premi la barra in linea retta fino alla posizione di partenza.",
    "Panca piana con manubri":
        "Sdraiati su una panca piana con un manubrio in ogni mano. Solleva i manubri uno alla volta in modo da poterli tenere davanti a te alla larghezza delle spalle. Usa il petto per sollevare i manubri.",
    "Push up":
        "Preparati con il peso sostenuto sulle dita dei piedi e le mani sotto le spalle, il corpo dritto. Fai attenzione a mantenere il tuo nucleo bloccato in modo che si formi una linea retta tra la testa, i glutei e i talloni. Abbassa il corpo fino a quando il petto si trova a un pollice da terra, quindi sali in modo esplosivo estendendo completamente le braccia.",
    "Rematore con bilanciere":
        "Tenendo un manubrio in ogni mano, piega leggermente le ginocchia e fai oscillare l'anca in modo che la parte superiore del corpo sia quasi parallela al pavimento. Mantieni il tuo nucleo stretto e la schiena dritta mentre remi i pesi fino al petto. Abbassa e ripeti.",
    "Rematore singolo con manubrio":
        "Dirigiti verso una panca piana e metti la mano destra contro di essa sotto la spalla, mantenendo il braccio dritto. Appoggia il ginocchio destro sulla panca e sposta l'altra gamba di lato. Con la mano libera prendi un manubrio dal pavimento e remalo al tuo fianco finché la parte superiore del braccio non è parallela al pavimento. Torna lentamente a terra e ripeti.",
    "Rematore singolo con barra":
        "Aggiungi peso a un'estremità di un bilanciere. Piegati in avanti finché il busto non è quasi parallelo al pavimento e tieni le ginocchia leggermente piegate. Afferra la barra con un braccio appena dietro i piatti. Tirare la barra verso l'alto con il gomito verso l'interno finché le piastre non toccano il petto e stringono i muscoli della schiena nella parte superiore del movimento. Abbassare lentamente alla posizione di partenza e ripetere senza che i piatti tocchino il pavimento.",
    "Squat":
        "Stai in piedi con i piedi alla larghezza delle spalle. Inizia il movimento piegando le ginocchia e sedendoti indietro con i fianchi. Scendi il più lontano possibile e inverti rapidamente il movimento tornando alla posizione iniziale. Tieni la testa alta e la schiena dritta durante il movimento.",
    "Affondi in camminata":
        "Affondo in avanti il più possibile con la gamba destra, piegando il ginocchio in avanti in modo che sfiora quasi il pavimento. Usa il tallone del piede destro per spingerti nel prossimo affondo, questa volta guidando con la gamba sinistra",
    "Ponte con gamba singola":
        "Sdraiati sulla schiena con una gamba sollevata in aria. Spingi in avanti e solleva i fianchi da terra il più in alto possibile. Contrai i glutei nella parte superiore della ripetizione per attivare più fibre muscolari e vedere una maggiore crescita. Abbassati lentamente sul pavimento",
    'Sumo Squat con Kettlbell':
        "Stai in piedi con i piedi più larghi della larghezza delle spalle e tieni un manubrio con entrambe le mani davanti al petto. Siediti in uno squat, quindi torna su e ripeti.",
    'Squat con bilanciere esplosivi':
        "Stai in piedi con i piedi più larghi della larghezza delle spalle e tieni un bilanciere sulla parte superiore della schiena con una presa prona, evita di appoggiarlo sul collo. Abbraccia la barra nelle trappole per impegnare i muscoli della parte superiore della schiena. Siediti lentamente in uno squat con la testa in alto, la schiena dritta e la schiena in fuori. Abbassati finché i fianchi non sono allineati con le ginocchia, con le gambe a 90 gradi. Spingi i talloni sul pavimento per spingerti indietro in modo esplosivo. Mantieni la forma finché non sei in piedi.",
    'Stacchi da terra con gambe tese':
        "Tieni un bilanciere davanti al tuo corpo con le ginocchia leggermente piegate. Abbassa il bilanciere fino alle caviglie estendendolo attraverso la vita e mantenendo la schiena dritta, quindi torna lentamente alla posizione di partenza.",
    "Swing con kettbell":
        "Piegati sui fianchi e tieni una kettlebell con entrambe le mani all'altezza delle braccia davanti a te. Oscillare leggermente all'indietro con la kettlebell tra le gambe. Quindi stringi i glutei, spingi i fianchi in avanti con forza e fai oscillare il peso all altezza delle spalle. Invertire il movimento tra le gambe e ripetere.",
    "Stacchi esplosivi con bilanciere":
        "Accovacciati e afferra un bilanciere con le mani alla larghezza delle spalle. Tieni il petto in alto, tira indietro le spalle e guarda dritto davanti a te mentre sollevi la barra. Concentrati sul riportare il peso sui talloni e mantieni la barra il più vicino possibile al tuo corpo in ogni momento. Sollevare a livello della coscia, fare una pausa, quindi tornare sotto controllo alla posizione di partenza.",
    "Pike":
        "Mettiti in una posizione di piegamento con le mani più larghe della larghezza delle spalle. Piegati in vita e solleva i talloni da terra, mantenendo la schiena dritta, in modo che il tuo corpo formi una forma a V capovolta. Piega i gomiti per abbassare la testa verso il pavimento. Quindi spingere indietro in modo esplosivo alla posizione di partenza.",
    "Rematore su panca":
        "Sdraiati a faccia in giù sulla panca con i piedi dall'altra parte per mantenerti stabile. Appendi i manubri sotto di te usando una presa neutra. Tieni la testa alta e unisci le scapole mentre remi i pesi verso il petto. Abbassare alla posizione di partenza sotto controllo",
    "Alzate laterali":
        "Stai in piedi tenendo un manubrio leggero in ogni mano. Solleva lentamente i manubri di lato finché non raggiungono l'altezza delle spalle, non più in alto, e resisti all'impulso di imbrogliare facendo oscillare il peso. Fai una pausa, quindi abbassati di lato, lentamente: costruirai più muscoli combattendo la gravità piuttosto che lasciare che faccia il lavoro per te.",
    "Spinte con bilanciere":
        "Afferra un bilanciere e tienilo all'altezza delle spalle con i palmi rivolti in avanti. Metti i piedi alla larghezza delle spalle e piega leggermente le ginocchia per iniziare il movimento. Spingi verso l'alto con le gambe per premere in modo esplosivo il bilanciere direttamente sopra la testa. Ritorna sotto controllo alla posizione di partenza.",
    "Alzate al mento":
        "In piedi (stazione eretta), busto diritto, addominali e glutei contratti per stabilizzare il bacino, impugnare i manubri con i palmi delle mani verso il corpo. Tirare i manubri verso le spalle sollevando i gomiti più alti delle spalle stesse.",
    "Curl con bicipiti inclinati":
        "Sedersi su una panca inclinata e tenere un manubrio in ogni mano a distanza di un braccio. Usa i bicipiti per alzare il manubrio finché non raggiunge la spalla, quindi abbassali di nuovo al tuo fianco e ripeti",
    "Curl concentrato":
        "Siediti su una panca e appoggia il braccio destro sulla gamba destra, lasciando che il peso penda. Alzare il peso, mettere in pausa, quindi abbassare. Ripetere con l'altro braccio.",
    "Dumbbell curl alternato":
        "Tieni un manubrio in ogni mano al tuo fianco con i palmi rivolti l'uno verso l'altro. Usa il bicipite per alzare alternativamente i manubri fino alle spalle, ruotando i palmi delle mani verso il petto mentre li sollevi. Abbassa lentamente i manubri di lato e ripeti",
    "Curl inversi con barra":
        "Stare in piedi e afferrare un bilanciere alla larghezza delle spalle con una presa prona. Fletti i gomiti e ruota il bilanciere verso l'alto, usando solo gli avambracci, finché i palmi delle mani sono rivolti verso l'esterno e il bilanciere è in linea con le spalle. Riportalo indietro lentamente e ripeti.",
    "Curl con barra EZ inclinato in avanti":
        "Tieni la barra EZ davanti alle cosce con una presa sottomano, alla larghezza delle spalle. Piegati leggermente in avanti, in modo che il busto sia di circa 30 gradi rispetto ai fianchi. Mentre inspiri, piega la barra fino a portare le mani sulle spalle. Contrai i bicipiti, quindi abbassati sotto controllo.",
    "Curl con barra EZ grip inverso":
        "Tieni la barra EZ davanti alle cosce con una presa prona, alla larghezza delle spalle. Mentre inspiri, piega la barra fino a portare le mani sulle spalle. Contrai i bicipiti, quindi abbassati sotto controllo.",
    "Curl su panca prono con manubri":
        "Sdraiati su una panca inclinata e tieni un manubrio in ogni mano, lasciandolo penzolare sotto le spalle. Usa i bicipiti per alzare i manubri verso le spalle. Ritorna lentamente alla posizione di partenza e ripeti.",
    "Burpees":
        "Stai in piedi con i piedi alla larghezza delle spalle. Abbassa il corpo finché i palmi delle mani non poggiano sul pavimento alla larghezza delle spalle. Calcia le gambe all'indietro in una posizione di piegamento, esegui un piegamento, quindi inverti rapidamente il movimento ed esegui un salto quando sei in piedi",
    "Jump squat":
        "Accovacciati, mantenendo la schiena dritta, finché le cosce non sono parallele al pavimento e il sedere è all'incirca all'altezza delle ginocchia. Esplodi verso l'alto in un salto e vai dritto nel prossimo squat.",
    "Affondi alternati con salto":
        "Affondo in avanti finché il ginocchio posteriore non tocca quasi il suolo. Salta in aria, portando il piede posteriore in avanti e il piede anteriore indietro. Atterra con un affondo e ripeti.",
    "Curl a trascinamento con barra EZ":
        "Tieni la barra EZ davanti alle cosce con una presa sottomano, alla larghezza delle spalle. Arriccia la barra e mentre le mani si alzano, porta indietro i gomiti in modo che la barra rimanga il più vicino possibile al busto. Contrai i bicipiti in alto, poi abbassati sotto controllo.",
    "Curl con manubri":
        "Tieni due manubri per la coscia, con i palmi rivolti lontano da te. Usa i bicipiti per sollevare i manubri fino a portarli all'altezza delle spalle, stringi il bicipite nella parte superiore del movimento. Abbassa la schiena e ripeti.",
    'Affondi con bilanciere':
        'Posiziona la barra sulla schiena. Fai un passo avanti con il piede destro e affonda in un affondo, in modo che entrambe le gambe siano piegate con il ginocchio posteriore il più vicino possibile al pavimento. Torna su e ripeti dall altra parte.',
    "Alzate al mento singolo":
        "Tieni un manubrio in una mano davanti al tuo corpo con una presa prona. Alza il gomito verso l'alto e di lato usando i muscoli delle spalle per sollevare il peso verso il mento. Abbassare sotto controllo fino alla posizione di partenza.",
    "Pressa spalle da seduto":
        "Sedersi sulla panca tenendo un bilanciere davanti alle spalle con una presa prona. Spingi il peso sopra la testa finché le braccia non sono completamente estese. Ritorna lentamente alla posizione di partenza.",
    "Panca inclinata con bilanciere":
        "Sdraiati su una panca inclinata e solleva un bilanciere all'altezza delle spalle, con i palmi rivolti verso di te. Espira mentre spingi verso l'alto con entrambe le braccia. Blocca le braccia e stringi il petto prima di tornare lentamente alla posizione di partenza.",
    "Pressa con manubri":
        "Stare in piedi tenendo due manubri all'altezza delle spalle con una presa prona – palmi rivolti in avanti. Assicurati che i gomiti siano davanti alla barra e non si allarghino ai lati. Spingere i pesi sopra la testa fino a quando le braccia non sono completamente estese. Ritorna lentamente alla posizione di partenza.",
    "Corda":
        "Afferra la corda ad entrambe le estremità. Usa i polsi per farlo scorrere intorno al tuo corpo, saltando per liberare la corda mentre colpisce il suolo. Rendi il movimento più intenso con i double under, lasciando che la corda ti passi intorno due volte per ogni salto.",
    "Squat con manubri":
        "Tenendo un manubrio in ogni mano, posiziona le gambe alla larghezza delle spalle. Mantenendo la testa alta e la schiena dritta, siediti nello squat finché i manubri non si trovano a pochi centimetri dal pavimento. Concentrati sul tenere le ginocchia sopra le dita dei piedi e il petto in fuori, non inarcare la schiena o piegarti in avanti mentre cadi. Espira, raddrizza le gambe e torna alla posizione di partenza.",
    "Deadbug":
        "Sdraiati sulla schiena con le mani sopra di te e i piedi sollevati in modo che le ginocchia siano a 90 gradi. Raddrizza la gamba fino a quando il tallone è a un pollice dal pavimento, quindi torna alla posizione di partenza. Ripeti con l'altra gamba.",
    "Pressa con manubri da terra":
        "Sdraiati sul pavimento con un manubrio in ogni mano. Piegati ai gomiti e tieni i pesi sopra di te. Premi e raddrizza le braccia prima di fermarti all'inizio della ripetizione e abbassarti lentamente fino alla posizione di partenza.",
    "Crunch":
        "Sdraiati sulla schiena con le ginocchia piegate a un angolo di 90 gradi. Metti le mani su entrambi i lati della testa. Spingi la parte bassa della schiena sul pavimento mentre sollevi le spalle di qualche centimetro dal pavimento, assicurati che la parte bassa della schiena rimanga sempre a contatto con il suolo. Contrai con forza gli addominali nel punto più alto del movimento, quindi torna sotto controllo alla posizione di partenza.",
    "Dorsali bassi a terra":
        "Sdraiati con le braccia lungo i fianchi. Solleva lentamente il petto verso l'alto, con le braccia in basso. Tieni la testa alta durante il movimento. Una volta raggiunto il punto più alto, abbassati di nuovo.",
    "Bear Crawls":
        "Trova un punto sul pavimento in cui hai circa 10 m di spazio vuoto. Mettiti sulle mani e sulle ginocchia, con il peso sui palmi e sulle dita dei piedi. Solleva le ginocchia da terra e mantieni la schiena piatta con un nucleo rinforzato. Muovi contemporaneamente la mano destra e il piede sinistro in avanti per iniziare a muoverti e viceversa. Guarda davanti a te e mantieni il tuo core rinforzato.",
    "Ponte":
        "Sdraiati sul pavimento con le gambe piegate. Guida attraverso i talloni per spingere i fianchi verso l'alto il più lontano possibile, prima di fermarti e tornare alla posizione di partenza.",
    "Diamond":
        "Mettiti in una posizione di flessione e unisci le mani in modo che indice e pollice formino un diamante. Tieni la schiena dritta mentre ti abbassi finché il petto non tocca quasi il pavimento, quindi spingi indietro fino alla posizione di partenza.",
    "Stacchi rumeni":
        "Mettiti dietro un bilanciere a terra. Piega leggermente le ginocchia per afferrarlo, mantenendo stinchi, schiena e fianchi dritti. Senza piegare la schiena, spingi i fianchi in avanti per sollevare la barra. Dalla posizione eretta, spingi indietro i fianchi per abbassare la barra, piegando solo leggermente le ginocchia.",
    "Sit up":
        "Sdraiati sul pavimento con le ginocchia piegate e, se possibile, aggancia i piedi sotto qualcosa che impedisca loro di muoversi. Metti le mani dietro la testa e contrai il busto mentre sollevi il busto in modo che la parte superiore del corpo formi una forma a V con le cosce. Abbassare sotto controllo fino alla posizione di partenza.",
    "Rematore inclinato con manubri":
        "Piegati con la schiena a 90 gradi e mantieni il tuo nucleo stretto e la schiena dritta mentre remi i pesi fino al petto.",
    "Renegade":
        "Mettiti in una posizione di piegamento con le mani sui manici di due manubri. Mantenendo il core teso, porta il manubrio destro fino agli addominali, quindi torna alla posizione di partenza. Ripeti con il manubrio sinistro per completare una ripetizione",
    "Superman":
        "Sdraiati prono su un materassino con le braccia distese davanti a te e le gambe distese dietro di te. Coinvolgendo i glutei e la parte bassa della schiena, solleva le braccia, le gambe e il petto dal pavimento. Mantieni la posizione per un conteggio, quindi torna lentamente alla posizione di partenza."
  };

  final Map<String, Map<String, String>> mapGiri = {
    'FULL BODY': {' Circuito 1': '', 'Circuito 2': '', 'Circuito 3': ''},
    'STRETCHING': {'Circuito 1': '', 'Circuito 2': ''},
    'LOW IMPACT': {'Circuito 1': '', 'Circuito 2': '', 'Circuito 3': ''}
  };

  getScheda(String nomeAllenamento) {
    Map<String, List> scheda = mapSchede[nomeAllenamento];
    return scheda;
  }

  getNumeroEserciziCircuitoScheda(String nomeAllenamento, int circuito) {
    Map<String, List> scheda = mapSchede[nomeAllenamento];
    int n = scheda['Circuito ' + circuito.toString()].length;
    return n;
  }

  getEsercizioCircuitoScheda(String nomeAllenamento, int circuito, int index) {
    Map<String, List> scheda = mapSchede[nomeAllenamento];
    String nomeEsercizio = scheda['Circuito ' + circuito.toString()][index];
    return nomeEsercizio;
  }

  getEsercizio(String nomeCircuito, String nomeAllenamento) {
    Map<String, String> scheda = getScheda(nomeAllenamento);
    String ese = scheda[nomeCircuito];
    return ese;
  }

  getNumeroCircuiti(String nomeAllenamento) {
    Map<String, List> scheda = getScheda(nomeAllenamento);
    int n = scheda.length;
    return n;
  }

  getRipetizioni(String nomeEsercizio) {
    String rip = mapRipetizioni[nomeEsercizio];
    return rip;
  }

  getImage(String nomeEsercizio) {
    String rip = mapImage[nomeEsercizio];
    return rip;
  }

  getGiri(String nomeAllenamento) {
    Map<String, String> giri = mapGiri[nomeAllenamento];
    return giri;
  }

  getDescrizione(String nomeEsercizio) {
    String d = des[nomeEsercizio];
    return d;
  }
}
