import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
//import 'package:syncfusion_flutter_charts/sparkcharts.dart';

class Progressi extends StatelessWidget {
  const Progressi({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(children: <Widget>[
      Container(
          child: Container(
              child: SfCartesianChart(
                  primaryXAxis: CategoryAxis(),
                  primaryYAxis: NumericAxis(labelFormat: '{value}kg'),
                  tooltipBehavior: TooltipBehavior(enable: true),
                  series: <ChartSeries>[
                    LineSeries<PesoMese, String>(
                        name: '',
                        dataSource: getChartDataPeso(),
                        xValueMapper: (PesoMese m, _) => m.mese,
                        yValueMapper: (PesoMese p, _) => p.peso,
                        dataLabelSettings: DataLabelSettings(isVisible: true),
                        color: Colors.greenAccent[700],
                        enableTooltip: true)
                  ],
                  title: ChartTitle(
                      text: 'Peso:',
                      textStyle: TextStyle(
                          color: Colors.greenAccent[700], fontSize: 22.0))))),
      Container(
          child: Container(
              child: SfCartesianChart(
                  primaryXAxis: CategoryAxis(),
                  primaryYAxis: NumericAxis(labelFormat: '{value}kcal'),
                  tooltipBehavior: TooltipBehavior(enable: true),
                  series: <ChartSeries>[
                    ColumnSeries<KcalMese, String>(
                        name: '',
                        dataSource: getChartDataKal(),
                        xValueMapper: (KcalMese m, _) => m.mese,
                        yValueMapper: (KcalMese k, _) => k.kcal,
                        dataLabelSettings: DataLabelSettings(isVisible: true),
                        color: Colors.greenAccent[700],
                        enableTooltip: true)
                  ],
                  title: ChartTitle(
                      text: 'Calorie:',
                      textStyle: TextStyle(
                          color: Colors.greenAccent[700], fontSize: 22.0)))))
    ])));
  }
}

List<PesoMese> getChartDataPeso() {
  final List<PesoMese> chartData = [
    PesoMese('Gen', 165),
    PesoMese('Feb', 62),
    PesoMese('Mar', 110),
    PesoMese('Apr', 37),
    PesoMese('Mag', 55),
    PesoMese('Giu', 45)
  ];
  return chartData;
}

List<KcalMese> getChartDataKal() {
  final List<KcalMese> chartData = [
    KcalMese('Lun', 165),
    KcalMese('Mar', 62),
    KcalMese('Mer', 110),
    KcalMese('Gio', 37),
    KcalMese('Ven', 55),
    KcalMese('Sab', 45)
  ];
  return chartData;
}

class KcalMese {
  KcalMese(this.mese, this.kcal);
  final String mese;
  final double kcal;
}

class PesoMese {
  PesoMese(this.mese, this.peso);
  final String mese;
  final double peso;
}
