import 'dart:io';
import 'package:fitness_app/fine_allenamento.dart';
import 'package:fitness_app/pausa_allenamento.dart';
import 'package:flutter/material.dart';
import 'package:fitness_app/allenamento.dart';
import 'package:flutter_speech/flutter_speech.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:countdown_flutter/countdown_flutter.dart';
import 'package:flutter_beep/flutter_beep.dart';
import 'package:breathing_collection/breathing_collection.dart';

const languages = const [
  const Language('Italiano', 'it_IT'),
];

class Language {
  final String name;
  final String code;

  const Language(this.name, this.code);
}

class InizioAllenamento extends StatefulWidget {
  final nomeAllenamento;
  final nCircuito;
  @override
  State<StatefulWidget> createState() => InitAllenamento();

  const InizioAllenamento({Key key, this.nomeAllenamento, this.nCircuito})
      : super(key: key);
}

class InitAllenamento extends State<InizioAllenamento> {
  int round = 0;
  int started = 0;
  String timerSeconds = "";
  SpeechRecognition _speech = SpeechRecognition();
  bool _speechRecognitionAvailable = false;
  bool _isListening = false;
  String transcription = '';
  Language selectedLang = languages.first;

  FlutterTts flutterTts;

  @override
  initState() {
    super.initState();
    String nomeAllenamento = widget.nomeAllenamento;
    int circuito = widget.nCircuito;
    activateSpeechRecognizer();
    flutterTts = FlutterTts();
    String lettura =
        Scheda().getEsercizioCircuitoScheda(nomeAllenamento, circuito, round) +
            Scheda().getRipetizioni(Scheda()
                .getEsercizioCircuitoScheda(nomeAllenamento, circuito, round));
    flutterTts.speak(lettura);
    String rip = Scheda().getRipetizioni(
        Scheda().getEsercizioCircuitoScheda(nomeAllenamento, circuito, round));
    if (rip.contains("secondi")) {
      flutterTts.speak("Preparati");
    }
  }

  void activateSpeechRecognizer() {
    print('_MyAppState.activateSpeechRecognizer... ');
    _speech = SpeechRecognition();
    _speech.setAvailabilityHandler(onSpeechAvailability);
    _speech.setRecognitionStartedHandler(onRecognitionStarted);
    _speech.setRecognitionResultHandler(onRecognitionResult);
    _speech.setRecognitionCompleteHandler(onRecognitionComplete);
    _speech.setErrorHandler(errorHandler);
    _speech.activate('it_IT').then((res) {
      setState(() => _speechRecognitionAvailable = res);
    });
  }

  @override
  Widget build(BuildContext context) {
    String nomeAllenamento = widget.nomeAllenamento;
    int circuito = widget.nCircuito;
    if (_speechRecognitionAvailable && !_isListening) start();
    //prende il numero degli esercizi del circuito
    int step = (round %
            Scheda()
                .getNumeroEserciziCircuitoScheda(nomeAllenamento, circuito)) +
        1;
    voiceControl();
    return Scaffold(
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(0, 10, 10, 5),
            child: Align(
                alignment: Alignment.topRight,
                child: GestureDetector(
                  onTap: () => {
                    Navigator.pushNamedAndRemoveUntil(
                        context, "/homepage", (route) => false)
                  }, // handle your image tap h
                  // ere
                  child: Image.asset('images/x_black.png',
                      scale: 25, alignment: Alignment.topRight),
                )),
          ),
          SizedBox(height: 10),
          Text(
            "CIRCUITO " + circuito.toString(), // circuito.toString(),
            style: TextStyle(
              fontSize: 30.0,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.w300,
              color: Colors.black,
              letterSpacing: 2.0,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "ESERCIZIO " +
                step.toString() +
                "/" +
                Scheda()
                    .getNumeroEserciziCircuitoScheda(nomeAllenamento, circuito)
                    .toString(),
            style: TextStyle(
              fontSize: 30.0,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.w300,
              color: Colors.black,
              letterSpacing: 2.0,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            Scheda().getEsercizioCircuitoScheda(nomeAllenamento, circuito,
                round), //mapRipetizioni.keys.elementAt(round),
            style: TextStyle(
              fontSize: 30.0,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold,
              color: Colors.black,
              letterSpacing: 2.0,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          buildtimer(),
          SizedBox(
            height: 20,
          ),
          Card(
            semanticContainer: true,
            clipBehavior: Clip.antiAliasWithSaveLayer,
            child: Image(
              image: AssetImage(Scheda().getImage(Scheda()
                  .getEsercizioCircuitoScheda(nomeAllenamento, circuito,
                      round))), //Scheda().mapImage.values.elementAt(round)),
              height: 250,
              width: 300,
              fit: BoxFit.fill,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            elevation: 5,
            margin: EdgeInsets.all(10),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            "A seguire: ",
            style: TextStyle(
              fontSize: 22.0,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.w300,
              color: Colors.black,
              letterSpacing: 2.0,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            round + 1 ==
                        Scheda()
                            .getScheda(nomeAllenamento)[
                                'Circuito ' + circuito.toString()]
                            .length &&
                    circuito == Scheda().getScheda(nomeAllenamento).keys.length
                ? "Fine allenamento"
                : (round + 1 ==
                        Scheda()
                            .getScheda(nomeAllenamento)[
                                'Circuito ' + circuito.toString()]
                            .length
                    ? "Riposo"
                    : Scheda().getEsercizioCircuitoScheda(
                        nomeAllenamento, circuito, round + 1)),
            style: TextStyle(
              fontSize: 30.0,
              fontStyle: FontStyle.italic,
              fontWeight: FontWeight.bold,
              color: Colors.black,
              letterSpacing: 2.0,
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Center(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ClipOval(
                  child: Container(
                    color: Colors.green,
                    child: IconButton(
                      padding: EdgeInsets.all(0),
                      icon:
                          Icon(Icons.arrow_left, color: Colors.white, size: 42),
                      onPressed: () {
                        if (round >= 1) {
                          round = round - 1;
                          String lettura = Scheda().getEsercizioCircuitoScheda(
                                  nomeAllenamento, circuito, round) +
                              Scheda().getRipetizioni(Scheda()
                                  .getEsercizioCircuitoScheda(
                                      nomeAllenamento, circuito, round));
                          flutterTts.speak(lettura);
                        }
                      },
                    ),
                  ),
                ),
                SizedBox(
                  width: 40,
                ),
                BreathingGlowingButton(
                  height: 30.0,
                  width: 30.0,
                  buttonBackgroundColor: Colors.transparent,
                  glowColor: Colors.green,
                ),
                SizedBox(
                  width: 40,
                ),
                ClipOval(
                  child: Container(
                    color: Colors.green,
                    child: IconButton(
                      padding: EdgeInsets.all(0),
                      icon: Icon(Icons.arrow_right,
                          color: Colors.white, size: 42),
                      onPressed: () {
                        //se round+1 è uguale al numero totale degli esercizi del circuito
                        if (round + 1 ==
                                Scheda()
                                    .getScheda(nomeAllenamento)[
                                        'Circuito ' + circuito.toString()]
                                    .length &&
                            circuito ==
                                Scheda()
                                    .getScheda(nomeAllenamento)
                                    .keys
                                    .length) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => FineAllenamento()),
                          );
                        } else if (round + 1 ==
                            Scheda()
                                .getScheda(nomeAllenamento)[
                                    'Circuito ' + circuito.toString()]
                                .length) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => PausaAllenamento(
                                    nomeAllenamento: nomeAllenamento,
                                    nCircuito: circuito)),
                          );
                        } else {
                          round = round + 1;
                          String lettura = Scheda().getEsercizioCircuitoScheda(
                                  nomeAllenamento, circuito, round) +
                              Scheda().getRipetizioni(Scheda()
                                  .getEsercizioCircuitoScheda(
                                      nomeAllenamento, circuito, round));
                          flutterTts.speak(lettura);
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          )
        ])));
  }

  buildtimer() {
    String nomeAllenamento = widget.nomeAllenamento;
    int circuito = widget.nCircuito;

    String rip = Scheda().getRipetizioni(
        Scheda().getEsercizioCircuitoScheda(nomeAllenamento, circuito, round));

    if (rip.contains("secondi")) {
      return CountdownFormatted(
          duration: Duration(seconds: 35),
          builder: (BuildContext ctx, String remaining) {
            if (int.parse(remaining) == 30) {
              timerSeconds = "VIA";

              flutterTts.speak("via");
              sleep(const Duration(milliseconds: 1));
            } else if (int.parse(remaining) > 30) {
              timerSeconds =
                  "tra" + " " + (int.parse(remaining).round() - 30).toString();
            } else {
              if (int.parse(remaining) % 5 == 0 && int.parse(remaining) < 30) {
                FlutterBeep.playSysSound(24);
              }
              if (int.parse(remaining) == 3 ||
                  int.parse(remaining) == 2 ||
                  int.parse(remaining) == 1) {
                FlutterBeep.playSysSound(24);
              }
              timerSeconds = remaining + " " + " secondi";
            }

            return Text(
              timerSeconds,
              style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.bold),
            );
          });
    } else {
      return Text(
        Scheda().getRipetizioni(Scheda()
            .getEsercizioCircuitoScheda(nomeAllenamento, circuito, round)),
        style: TextStyle(
          fontSize: 28.0,
          fontStyle: FontStyle.italic,
          fontWeight: FontWeight.bold,
          color: Colors.black,
          letterSpacing: 2.0,
        ),
      );
    }
  }

  void start() => _speech.activate(selectedLang.code).then((_) {
        return _speech.listen().then((result) {
          print('_MyAppState.start => result $result');
          setState(() {
            _isListening = result;
          });
        });
      });

  void cancel() =>
      _speech.cancel().then((_) => setState(() => _isListening = false));

  void stop() => _speech.stop().then((_) {
        setState(() => _isListening = false);
      });

  void onSpeechAvailability(bool result) =>
      setState(() => _speechRecognitionAvailable = result);

  void onCurrentLocale(String locale) {
    print('_MyAppState.onCurrentLocale... $locale');
    setState(
        () => selectedLang = languages.firstWhere((l) => l.code == locale));
  }

  void onRecognitionStarted() {
    setState(() => _isListening = true);
  }

  void onRecognitionResult(String text) {
    print('_MyAppState.onRecognitionResult... $text');
    setState(() => transcription = text);
  }

  void onRecognitionComplete(String text) {
    print('_MyAppState.onRecognitionComplete... $text');
    setState(() => _isListening = false);
  }

  void errorHandler() => {
        _speechRecognitionAvailable = false,
        _isListening = false,
        activateSpeechRecognizer(),
        //start()
      };

  voiceControl() async {
    String nomeAllenamento = widget.nomeAllenamento;
    int circuito = widget.nCircuito;
    if (transcription.contains("prossimo") && _isListening == false) {
      if ((round + 1) ==
              Scheda()
                  .getScheda(nomeAllenamento)['Circuito ' + circuito.toString()]
                  .length &&
          circuito == Scheda().getScheda(nomeAllenamento).keys.length) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => FineAllenamento()),
        );
      } else if (round + 1 ==
          Scheda()
              .getScheda(nomeAllenamento)['Circuito ' + circuito.toString()]
              .length) {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => PausaAllenamento(
                  nomeAllenamento: nomeAllenamento, nCircuito: circuito)),
        );
      } else {
        round = round + 1;
        cancel();
        transcription = '';
        String lettura = Scheda()
                .getEsercizioCircuitoScheda(nomeAllenamento, circuito, round) +
            Scheda().getRipetizioni(Scheda()
                .getEsercizioCircuitoScheda(nomeAllenamento, circuito, round));
        flutterTts.speak(lettura);
        sleep(const Duration(seconds: 1));
      }
      print(
          "**************************************************" + transcription);
    } else if (transcription.contains("termina")) {
      Navigator.pushNamedAndRemoveUntil(context, "/homepage", (route) => false);
    } else if (transcription.contains("indietro")) {
      if (round >= 1) {
        round = round - 1;
        cancel();
        transcription = '';
        String lettura = Scheda()
                .getEsercizioCircuitoScheda(nomeAllenamento, circuito, round) +
            Scheda().getRipetizioni(Scheda()
                .getEsercizioCircuitoScheda(nomeAllenamento, circuito, round));
        flutterTts.speak(lettura);
        sleep(const Duration(seconds: 1));
      }
    }
  }
}
